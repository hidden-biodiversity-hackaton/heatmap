FROM node:17.8.0-alpine

RUN apk update && apk add --no-cache

WORKDIR /app

COPY package*.json ./

RUN npm install

RUN chown -R node /app/node_modules

USER node

CMD ["npm", "run", "dev"]
