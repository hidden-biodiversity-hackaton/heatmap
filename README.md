# hidden-biodiversity

## Requirements
[Docker engine](https://docs.docker.com/engine/install/)  
[Docker compose](https://docs.docker.com/compose/install/)

## Project Setup
To run the hot reload development server:
```sh
docker-compose up
```

To add a new npm package:
```sh
docker exec hiddenBioWeb npm install [packageName]
```